*** Settings ***
Library           Selenium2Library

#TODO: Add a Suite Setup step to automatically open the browser up for UI Testing
Suite Setup     Open Browser    https://www.t2systems.com/home


#TODO: Add a Suite Teardown step to automatically close the browser when suite is finished
Suite Teardown     Close Browser



*** Test Cases ***


#TODO: Change all Open Browser keywords to 'Go to'

Browser Test 1
	Go to        https://www.google.ca/
    Title Should Be     Google


Browser Test 2
	Go to        https://www.youtube.com/
    Title Should Be     YouTube


Browser Test 3
	Go to        https://www.python.org/
    Title Should Be     Welcome to Python.org


Browser Test 4
	Go to        http://robotframework.org/
    Title Should Be     Robot Framework


Browser Test 5
	Go to        https://qa4-iris.t2systems.com/
    Title Should Be     Digital Iris - Sign in


Browser Test 6
	Go to        http://www.seleniumhq.org/
    Title Should Be     SeleniumHQ Browser Automation








