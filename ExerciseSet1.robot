*** Settings ***

## TODO: Exercise 1: Import Selenium2Library to successfully run Exercise 1 Test
#Library    Selenium2Library
#TODO: Exercise 2: Create a robot file named data.robot, declare a variable called browser and save a browser name to it, and import the data.robot resource here
Resource    data.robot



*** Test Cases ***

Exercise 1 Test  # Should run if Selenium2Library is imported correctly under Settings
	Open Browser        https://www.google.ca/    firefox
    Title Should Be     Google

Exercise 2 Test
    [Documentation]  TODO: Exercise 2: Copy the test case script used in Exercise 1, and replace the hardcoded URL(declared in this file)
    ...  and browser(declared in data.robot, add Documentation header in settings to specify its for keywords) with variables
    Open Browser        ${URL}     ${Browser}
    Title Should Be     Digital Iris - Sign in


Exercise 3 Test
    [Documentation]  TODO: Using Selenium2Library as a resource as well as everything learned from Exercises 1 and 2,
    ...  create a script that will open up the browser, navigate to the Iris Login URL and confirm the title,
    ...  login as a customer admin and verify that the dashboard page is open by checking the title.
    ...  For exercise purposes, try to open brower to random site (i.e. google.com) then navigate to Iris Login URL
# Hint: Keywords needed: Open Browser, Input Text, Title Should be, Go to, Location Should Be
    Open Browser        https://www.google.ca/       ${Browser}
    Go to       ${URL}
    Title should be     Digital Iris - Sign in
    Input Text  css=#username   ${Username}
    Input Text  css=#password   ${Password}
    Click Button    css=#submit
    Title should be     Digital Iris - Main (Dashboard)



Exercise 4 Test
    [Documentation]  TODO: Robust the test script written in Exercise 3 into re-usable keywords and define it under Keywords,
    ...  either internally or as an external file. Then construct the same test as Exercise 3, using only the newly defined keywords.
    ...  *Bonus* Move the keywords than to the external data.robot file created in Exercise 2, under the same Keywords header
    ...  and then re-run the test. If it passed before, it should also pass here as well.
    ...  Keyword for submitting credentials will already be provided as a sample
    Open Browser to Iris Login Page
    Login Page Should Be Open
    Input Username
    Input Password
    Submit Credentials
    Customer Admin Welcome Page Should Be Open



