*** Settings ***
Library           Selenium2Library


Suite Setup       Open Browser to Iris Login Page
Suite Teardown    Close Browser


#TODO: Add a Test Setup step to automatically reload the page before every test
Test Setup      Go To       ${LOGIN URL}
#Since Reload is not working properly, the alert box cannot be closed, use "Go To"  keyword instead of "Reload"

#TODO: Add a test template step to be used as a template for runnning each test
Test Template       InvalidLogin


*** Variables ***
${SERVER}         qa4-iris.t2systems.com
${BROWSER}        chrome
${LOGIN URL}      https://${SERVER}/


*** Test Cases ***


# TODO: Re-factor these test steps into a re-usable template and change the test cases into a tabular format

Invalid Login 1     billyboy     1233213123

Invalid Login 2     admin@oranj     sdfsdfsfdsfsdfsdfsd

Invalid Login 3     werwrwerfs      Password$1

Invalid Login 4     admin@oranj     ${EMPTY}


*** Keywords ***


Alert Message Should Contain
	Element Should Contain    xpath=.//*[@id="messageResponseAlertBox"]/section     Attention

Open Browser to Iris Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    0
    Login Page Should Be Open

Login Page Should Be Open
    Title Should Be    Digital Iris - Sign in


#TODO: Create new keyword that will act as a template for testing
InvalidLogin
    [Arguments]     ${username}     ${password}
	Input Text      css=#username    ${username}
	Input Text      css=#password    ${password}
	Click Button    submit
	Alert Message Should Contain







  