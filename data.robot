*** Settings ***

# TODO: Exercise 1: Import Selenium2Library to successfully run Exercise 1 Test
Library    Selenium2Library

*** Variables ***
# TODO: Declare Variable for URL here
${URL}      https://qa4-iris.t2systems.com/
${Browser}    chrome
${Username}     Han Company
${Password}     Welcome!123


*** Keywords ***

Submit Credentials
    Click Button    submit

Input Username
#TODO: Exercise 4: Create Keyword with one argument that takes in a username and inputs it into the username field of Iris Login
    Input Text  css=#username   ${Username}

Input Password
#TODO: Exercise 4: Create Keyword with one argument that takes in a password and inputs it into the password field of Iris Login
    Input Text  css=#password   ${Password}

Login Page Should Be Open
#TODO: Exercise 4: Create Keyword that simply checks the current page location is the url of the iris login page and the title is Digital Iris - Login
    Title should be     Digital Iris - Sign in

Go To Login Page
#TODO: Exercise 4: Create Keyword that simply navigates browser to login page
    Go to       ${URL}

Customer Admin Welcome Page Should Be Open
#TODO: Exercise 4: Create keyword that after successful login, verifies the page location and title is at the dashboard
    Title should be     Digital Iris - Main (Dashboard)

Open Browser to Iris Login Page
#TODO: Exercise 4: Create Keyword that opens up the browser, maximizes the browser window, set selenium speed (already provided),
#TODO: and navigate to Iris Login URL (Using the Go To Login Page keyword. (Note: Keywords can call other keywords so long as imported correctly in Settings))
#	Set Selenium Speed    0
    Open Browser        https://robotframework.org/     ${Browser}
    Maximize Browser Window
    Go To Login Page
    Set Selenium Speed      0


